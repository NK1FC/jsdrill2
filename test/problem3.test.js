
const problem3 = require('../problem3.cjs');

const inventory = [
  { "id": 2, "car_make": "Mazda", "car_model": "Miata MX-5", "car_year": 2001 },
  { "id": 3, "car_make": "Land Rover", "car_model": "Defender Ice Edition", "car_year": 2010 },
  { "id": 4, "car_make": "Honda", "car_model": "Accord", "car_year": 1983 },
  { "id": 5, "car_make": "Mitsubishi", "car_model": "Galant", "car_year": 1990 }
]


test('problem3', () => {
  expect(problem3(inventory)).toStrictEqual(
    ['Accord', 'Defender Ice Edition', 'Galant', 'Miata MX-5']
    );
});