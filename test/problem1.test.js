

const inventory = require('../inventory.cjs')
const problem1 = require('../problem1.cjs')


test('problem1', () => {
    expect(problem1(inventory, 33)).toStrictEqual({
        id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011
    });
});


