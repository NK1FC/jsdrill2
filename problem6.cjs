// ==== Problem #6 ====
/* A buyer is interested in seeing only BMW and Audi cars within the inventory.  
Execute a function and return an array that only contains BMW and Audi cars.  
Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.*/

const  inventory  = require('./inventory.cjs');

function problem6(inventory) {
    try {
        const filtercar = inventory.filter((a)=>{
            return a.car_make=='BMW'? true : a.car_make=='Audi'? true : false;
        });
        return filtercar;
    } catch (err) {
        return [];
    }
    return [];
}

console.log(problem6(inventory))

module.exports = problem6;
